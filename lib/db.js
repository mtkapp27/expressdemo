const mysql = require('mysql')

class Db { 
	constructor() {
		this.pool = mysql.createPool({
			host: "localhost",
			user: "root",
			password: "changeme",
			database: "expressDemo"
		})
	}

	insertUser(user){ 
		this.pool.getConnection((err, conn) => {
			if(err) throw err
			conn.query('INSERT INTO users SET ?', user, (err, results) => {
				if(err) throw err
			})
			conn.release()

		})
	}
}

module.exports = new Db()
