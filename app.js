const express = require('express') //web server framework (http)
const path = require('path')
const bodyparser = require('body-parser')

const routes = require('require-all') (path.join(__dirname, '/routes') ) 

const app = express()
app.set('views', path.join(__dirname, '/views') )
app.set('view engine', 'pug')

app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false} )) 


//home page
//request (req) and response (res)
//app.get('/user/add', function () {} )
app.get('/user/add', routes.user.add)
app.post('/user/add', routes.user.add)

//redirect to user/add
app.get('/', (req, res) => { res.redirect('/user/add') } )

//search page
//app.get('/user/search', (req, res) => {
//	res.send('search page')
//} )

app.get('/user/search', routes.user.search)



app.listen(3002)


