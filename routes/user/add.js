const Db = require('../../lib/db')

module.exports = function (req, res) {
	if (req.method === 'GET') {
		res.render('user/add')
	}
	if (req.method === 'POST') {
		Db.insertUser(req.body) 
		res.render('user/add', req.body)
	}
}
